import React, { useEffect } from "react"
import { Switch, Route } from "react-router-dom"
import { Home } from "./components/Home"
import { Header } from "./components/Header"
import { Products } from "./components/Products"
import { ProductsCalcas } from "./components/Products/Pants/ProductsPants"
import { ProductsShoes } from "./components/Products/Shoes/ProductsShoes"
import { ProductsClothes } from "./components/Products/Categories/Clothes/ProductsClothes"
import { ProductsWalking } from "./components/Products/Categories/Walking/ProductsWalking"
import { ProductsRance } from "./components/Products/Categories/Race/ProductsRace"
import { ProductsCasual } from "./components/Products/Categories/Casual/ProductsCasual"
import { ProductsSocial } from "./components/Products/Categories/Social/ProdutsSocial"

export function Rotas() {
    
    const [colorButton, setColorButton]= React.useState(false)

    return (
        <Switch>
            <>
                <Header BackgroundColor={colorButton} onchangeBackgroundColor={setColorButton} />
                <Route exact path='/'>
                    <Home />
                </Route>

                <Route exact path='/camisetas'>
                    <Products onchangeBackgroundColor={setColorButton} />
                </Route>

                <Route exact path='/calcas'>
                    <ProductsCalcas onchangeBackgroundColor={setColorButton}/>
                </Route>

                <Route exact path='/calcados'>
                    <ProductsShoes onchangeBackgroundColor={setColorButton}/>
                </Route>

                <Route exact path='/roupas'>
                    <ProductsClothes onchangeBackgroundColor={setColorButton}/>
                </Route>

                <Route exact path='/caminhada'>
                    <ProductsWalking onchangeBackgroundColor={setColorButton}/>
                </Route>

                <Route exact path='/corrida'>
                    <ProductsRance onchangeBackgroundColor={setColorButton}/>
                </Route>

                <Route exact path='/casual'>
                    <ProductsCasual onchangeBackgroundColor={setColorButton}/>
                </Route>

                <Route exact path='/social'>
                    <ProductsSocial onchangeBackgroundColor={setColorButton}/>
                </Route>
            </>
        </Switch>
    )
}