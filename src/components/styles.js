import styled,{ createGlobalStyle } from "styled-components"

export const GlobalStyle = createGlobalStyle`
*{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

html {
    @media (max-width: 1080px){
        font-size: 93.75%;
    }
    @media (max-width: 720px) {
        font-size: 87.75%;
    }
}

body{
    background: #FFFFFF 0% 0% no-repeat padding-box;
    color: #FFFFFF;
    font-family: 'Open Sans', sans-serif;
    -webkit-font-smoothing: antialiased;
}

a{
    color: #FFFFFF;
}

button{
    font-size: 1rem;
    cursor: pointer;
}
`
export const ContainerRoute = styled.main`
    display: grid;
    grid-template: auto auto/ 21% auto ;
    gap: 27px;
    /* background: green; */
    margin: 1rem 6.18rem 0 6.31rem;

    .localizacao{
        color: black;
        height: .1rem;
        grid-column: 1/3;
    }

   @media(max-width: 768px){
        gap: 26px;
        margin: 1rem 3rem 0 2.31rem;
        width: 48rem;
   } 

   @media(max-width: 320px){
       display: block;
       margin: auto;
       width: 320px;
   }
`
