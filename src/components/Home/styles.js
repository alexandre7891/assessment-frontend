import styled from "styled-components"

export const ContainerHome = styled.section`
    display: grid;
    grid-template: auto auto/ 21% auto ;
    gap: 10px;
    /* background: green; */
    margin: 1rem 6.18rem 0 6.31rem;

    .localizacao{
        color: black;
        height: .1rem;
        grid-column: 1/3;
    }
   
   
`
export const ContainerNavSide = styled.nav`
    background: #E2DEDC;
    font-weight: bolder;

    grid-row:  2/4;

    a{
        text-decoration: none;
        color: black;
        transition: padding linear 1ms;
        &:hover{
            padding: 2px;
        }
    }

    ul{
        list-style: none;
        padding: 1.4rem 0 0 1.1rem;
        li{
            margin-top: .5rem;
        }
    }

    @media(max-width: 768px){
        width: 9.5rem;
    }

    @media(max-width: 320px){
        display: none;
    }
`
export const Footer = styled.footer`
    background: #CB0D1F;
    grid-column: 1/3;
    height: 11rem;

    @media(max-width: 320px){
        width: 20rem;
        margin: auto;
        margin-top: 1rem;
    }
`

export const FristFieldHome = styled.div`
   background: #ACACAC;
   height: 17.56rem;
   grid-row: 2/2;

   @media(max-width: 320px){
       width: 18.125rem;
       height: 5.375rem;
       margin:auto;
   }
`

export const Welcome = styled.div`
        grid-column: 2/3;
        color: #000000;
        height: auto;
        div{
            font-size: 1.625rem;
            margin: .8rem 0;
        }
        @media(max-width: 320px){
            width: 18.125rem;
            margin: auto;
        }
`
