import React from "react"
import { ContainerRoute } from "../styles"
import { SideNavigation } from "./SideNavigation"
import { Footer, FristFieldHome } from "./styles"
import { WelcomeHome } from "./Welcome"

export function Home() {
   
    return (
        <ContainerRoute>
            <div className="localizacao"></div>
            <SideNavigation />
            <FristFieldHome />
           <WelcomeHome/>
            <Footer />
        </ContainerRoute>
    )
}