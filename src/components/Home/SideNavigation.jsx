import React, { useEffect } from "react"
import { BrowserRouter, Link } from "react-router-dom"
import { ContainerNavSide } from "./styles"

export function SideNavigation() {

    const [categories, setCategories] = React.useState([{}])

    useEffect(() => {
        fetch('http://localhost:8888/api/v1/categories/list')
            .then(response => response.json())
            .then(data => setCategories(data.items))
    }, [])

    return (
            <ContainerNavSide>
                <ul>
                    <li>
                        <Link to='/'>
                        PÁGINA INICIAL
                        </Link>
                    </li>
                    {categories?.map((category) => (
                        <li key={category.id}>
                            <Link to={category.path}>
                                {category.name?.toUpperCase()}
                            </Link>

                        </li>
                    ))}
                </ul>
            </ContainerNavSide>
    )
}