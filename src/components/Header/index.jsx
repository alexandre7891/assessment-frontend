import React from "react"
import { HeaderLogo } from "./HeaderLogo"
import { HeaderSession } from "./HeaderSession"
import { NavHeader } from "./NavHeader"

export function Header({ BackgroundColor, onchangeBackgroundColor }) {

    return (
        <header>
            <HeaderSession />
                <HeaderLogo handleChangeColorButton={BackgroundColor} onchangeBackgroundColor={onchangeBackgroundColor} />
            <NavHeader onchangeBackgroundColor={onchangeBackgroundColor} />
        </header>
    )
}