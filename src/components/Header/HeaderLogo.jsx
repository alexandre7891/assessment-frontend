import React from "react"
import { ButtonHeader, ButtonMobile, ContainerInput, ContainerLogoSearch, ImgMenuMobile } from "./styles"
import LogoImg from "../../../public/media/logo.jpg"
import LogoMobileImg from "../../../public/media/logoMobile.jpg"
import CarImg from "../../../public/media/car.jpg"
import { Link } from "react-router-dom/cjs/react-router-dom.min"
import { MenuMobile } from "./MenuMobile"

export function HeaderLogo({ handleChangeColorButton, onchangeBackgroundColor }) {

    const [text, setText] = React.useState('')
    const [menuEffect, setMenuEffect] = React.useState(false)
    const handleCloseMenu = (value) => {
        setMenuEffect(value)
    }
    return (
        <>

            <ContainerLogoSearch>

                <ButtonMobile onClick={() => { setMenuEffect(!menuEffect) }} MenuEffect={menuEffect} />

                <Link to='/'>
                    <img src={LogoImg} alt="Logo da webjump" onClick={() => onchangeBackgroundColor(false)} className="logo" />
                    <img src={LogoMobileImg} alt="Logo mobile da webjump" className="logoMobile"/>
                </Link>

                <img src={CarImg} alt="carrinho de compras" onClick={() => alert('Em breve...')} className="logoMobile"/>

                <ContainerInput>
                    <input type='text' onChange={event => setText(event)} />
                    <ButtonHeader type="button" handleChangeColorButton={handleChangeColorButton}>
                        BUSCAR
                    </ButtonHeader>
                </ContainerInput>

            </ContainerLogoSearch>

            <MenuMobile MenuEffect={menuEffect} CloseMenu={handleCloseMenu}/>
        </>
    )
}