import styled, { css } from "styled-components"

export const AreaLogin = styled.nav`
    background: #231F20 0% 0% no-repeat padding-box;
    padding: 0.2rem 6.18rem 0.42rem 0;
    div{
        text-align: end;
        font-size: 0.875rem;
    }

    @media(max-width: 768px){
        padding: 0.2rem 1.5rem 0.42rem 0;
    }

    @media(max-width: 320px){
        div{
            text-align: center;
        }        

    }
`

export const ContainerLogoSearch = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 1.5rem 6rem;

    a{
        img{
            height: 3.27rem;
           
        }   
    }

    .logo{
        display: block;
    }

    .logoMobile{
        display: none;
    }

    @media(max-width: 768px){
        margin: 1.5rem 1.5rem;
    }

    @media(max-width: 320px){
        justify-content: space-evenly;

        .logo{
            display: none;
        }
        .logoMobile{
            display: block;
        }

        a{
            margin: auto;
           
        }
    }
    
`
export const ContainerMenuMobile = styled.nav`

    display: none;
    font-weight: bolder;
    
    background: transparent;
    position: absolute;
    height: 1vh;
    width: 320px;

    transition: all linear 0.2s;

    a{
        text-decoration: none;
        
        color: transparent;
    }

    ul{
        list-style: none;
        position: sticky;
        
        li{
            margin-top: 2.5rem;
            text-align: center;
            font-size: 1.625rem;
        }
    }

    ${({MenuEffect})=>
        MenuEffect && css`
            background: #CB0D1F 0% 0% no-repeat padding-box;
            height: 170vh;
            a{
                color: black;
            }
        `
    }

    @media(max-width: 320px){
        display: block;
    }

`

export const ButtonMobile = styled.div`
display: none;

@media(max-width: 320px){
    display: block;
    width: 27px;
    border-top: 2px solid;
    transition: all linear .3s;
    color: black;
    

    &::after{
        content: '';
        display: block;
        width: 27px;
        height: 2px;
        margin-top: 9px;
        background-color: black;
        ${({MenuEffect})=> 
            MenuEffect && css`
            transform: rotate(-135deg);
            position: relative;
            top: -10px;
            `
        }
    }

    &::before{
        content: '';
        display: block;
        width: 27px;
        height: 2px;
        margin-top: 9px;
        background-color: black;

        ${({MenuEffect})=> 
            MenuEffect && css`
            transform: rotate(135deg);
            `
        }
    }

    ${({MenuEffect})=> 
            MenuEffect && css`
            color:transparent;
            `
        }

}
`

export const ContainerInput = styled.div`
    display: flex;
    align-items: center;

    width: 32.75rem;
    height: 2.75rem;
    
    input{
        outline: none;

        padding: 5px;
        width: 25.87rem;
        height: 2.75rem;
    }

    @media(max-width: 320px){
        display: none;
    }

`

export const ButtonHeader = styled.button`
        height: 2.75rem;
        width: 6.87rem;
        padding: 0.67rem 1.58rem 0.69rem 1.47rem;
        background: #CB0D1F 0% 0% no-repeat padding-box;
        font-weight: 800;
        border: none;
        color: #FFFF;
        transition: all linear 0.1s;

        ${({handleChangeColorButton})=>
            handleChangeColorButton ? css`
                background: #00A8A9 0% 0% no-repeat padding-box;
            `:css`
                background: #CB0D1F 0% 0% no-repeat padding-box;
            `
        }
`

export const ContainerNav = styled.nav`
    background: #CB0D1F 0% 0% no-repeat padding-box;
    font-weight: 800;
    padding: 1.12rem 0 .87rem 6.8rem;

    ul{
        display: flex;
        list-style: none;
        

        li{
            margin-right: 4.68rem;
        }
        
        a{
            font-weight: 800;
            text-decoration: none;
            font-size: 1rem;
        }
    }

    @media(max-width: 768px){

        padding: 1.12rem 0 .87rem 2.5rem;
        ul{
            li{margin-right: 3rem;}
        }
    }

    @media(max-width: 320px){
        display: none;
    }

`