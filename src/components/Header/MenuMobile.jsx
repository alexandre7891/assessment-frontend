import React, { useEffect } from "react"
import { Link } from "react-router-dom"
import { ContainerMenuMobile } from "./styles"


export function MenuMobile({MenuEffect,CloseMenu}) {
    const [categories, setCategories] = React.useState([{}])

    useEffect(() => {
        fetch('http://localhost:8888/api/v1/categories/list')
            .then(response => response.json())
            .then(data => setCategories(data.items))
    }, [])
    return (
        <ContainerMenuMobile MenuEffect={MenuEffect}>
            <ul>
                <li>
                    <Link to='/' onClick={()=>CloseMenu(false)}>
                        PÁGINA INICIAL
                    </Link>
                </li>

                {categories?.map((category) => (
                    <li key={category.id}>
                        <Link to={category.path} onClick={()=>CloseMenu(false)}>
                            {category.name?.toUpperCase()}
                        </Link>
                    </li>
                ))}

                <li>
                    <a>
                        CONTATO
                    </a>
                </li>
            </ul>

        </ContainerMenuMobile>
    )
}