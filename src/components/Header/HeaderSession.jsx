import React from "react"
import { AreaLogin } from "./styles"

export function HeaderSession() {
    return (
        <AreaLogin>
            <div>
                <a href="#"> Acesse sua Conta </a>
                <span>ou </span>
                <a href="#"> Cadastre-se</a>
            </div>
        </AreaLogin>
    )
}