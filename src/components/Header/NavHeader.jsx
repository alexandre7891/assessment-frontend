import React, { useEffect } from "react"
import { Link } from "react-router-dom"

import { ContainerNav } from "./styles"

export function NavHeader({onchangeBackgroundColor}) {
    const [categories, setCategories] = React.useState([{}])

    useEffect(() => {
        fetch('http://localhost:8888/api/v1/categories/list')
            .then(response => response.json())
            .then(data => setCategories(data.items))
    }, [])

    return (
        <ContainerNav>
            <ul>
                <li>
                    <Link to='/' onClick={()=>onchangeBackgroundColor(false)} >
                        PÁGINA INICIAL
                    </Link>
                </li>

                {categories?.map((category) => (
                    <li key={category.id}>
                        <Link to={category.path} >
                            {category.name?.toUpperCase()}
                        </Link>
                    </li>
                ))}

                <li>
                    <a>
                        CONTATO
                    </a>
                </li>

            </ul>
        </ContainerNav>
    )
}