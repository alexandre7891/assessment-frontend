import React from "react"
import { Link } from "react-router-dom"
import { ContainerColors, ContainerNavSideProducts, ContainerTypes } from "./styles"
import { ContainerLinks, H1, H3 } from "./styles"

export function SideNavigationProducts({ ShowColors, GetColorFilter }) {

    return (
        <ContainerNavSideProducts>
            <H1>FILTRE POR</H1>

            <H3>CATEGORIAS</H3>

            <ContainerLinks>
                <li>
                    <Link to='/roupas'> Roupas </Link>
                </li>
                <li>
                    <Link to='calcados'>Sapatos</Link>
                </li>
                <li>
                    <a href="#" onClick={() => alert('Em breve...')}> Acessórios </a>
                </li>
            </ContainerLinks>

            <H3>CORES</H3>

            <ContainerColors>
                <li>
                    <div className="Preto" onClick={(event) => GetColorFilter(event.target.className)} />
                </li>

                {!ShowColors && (
                    <>
                        <li>
                            <div className="Laranja" onClick={(event) => GetColorFilter(event.target.className)} />
                        </li>

                    </>
                )}

                <li>
                    <div className="Rosa" onClick={(event) => GetColorFilter(event.target.className)} />
                </li>

                <li>
                    <div className="Amarela" onClick={(event) => GetColorFilter(event.target.className)} />
                </li>

                {ShowColors && (
                    <>
                        <li>
                            <div className="Azul" onClick={(event) => GetColorFilter(event.target.className)} />
                        </li>

                        <li>
                            <div className="Cinza" onClick={(event) => GetColorFilter(event.target.className)} />
                        </li>
                        <li>
                            <div className="Bege" onClick={(event) => GetColorFilter(event.target.className)} />
                        </li>

                    </>
                )}

            </ContainerColors>

            <H3>TIPO</H3>

            <ContainerTypes>
                <li>
                    <Link to='/corrida'> Corrida</Link>
                </li>
                <li>
                    <Link to='/caminhada'> Caminhada </Link>
                </li>
                <li>
                    <Link to='/casual'>
                        casual
                    </Link>
                </li>
                <li>
                    <Link to='/social'> Social </Link> 
                </li>
            </ContainerTypes>

        </ContainerNavSideProducts>
    )
}