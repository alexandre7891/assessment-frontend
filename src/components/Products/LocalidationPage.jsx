import React from "react"
import { Link } from "react-router-dom/cjs/react-router-dom.min"
import { ContainerLocalisation } from "./styles"

export function Localisation({ onchangeBackgroundColor }) {
    const route = window.location.pathname.substring(1)
    return (
        <ContainerLocalisation>
            <Link to='/'
                onClick={() => onchangeBackgroundColor(false)}
            >
                Página inicial {'> '}
            </Link>
            <span>
                {route === 'calcas' ? 'Calças' : (route === 'calcados' ? 'calçados' : route)}
            </span>
        </ContainerLocalisation>
    )
}