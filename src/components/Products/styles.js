import styled from "styled-components"

export const ContainerNavSideProducts = styled.nav`

    background: #FFFFFF 0% 0% no-repeat padding-box;
    border: 1px solid #E2DEDC;
    font-weight: bolder;
    padding: 1.4rem 0 0 1.1rem;

    grid-row:  2/4;

    height: 29.62rem;

    @media(max-width: 768px){
        width: 11rem;
    }

    @media(max-width: 320px){
        width: 20rem;
        margin:auto;
    }

`

export const H1 = styled.h1`
    color: #ED1A39;
    
`
export const H3 = styled.h3`
    color: #1E2B50;
    margin-top: 1.43rem;
`

export const ContainerColors = styled.ul`
    display: flex;
    flex-wrap: wrap;
    list-style: none;
    
    /* background: bisque; */
    margin-top: 0.49rem;
    li{
        margin-right: 3px;
        margin-top: 0.2rem;

        div{
            width: 2rem;
            height: 1.5rem;
        }

        .Preto{background: black}

        .Laranja{background: #F26324}

        .Amarela{background:#FFFF00;}

        .Rosa{background:#ffc0cb}

        .Azul{background: blue;}

        .Cinza{background:gray;}

        .Bege{background:beige;}
    }
`

export const ContainerLinks = styled.ul`
    color: #626262;
    
    margin: 0.69rem 0 0 1.06rem;

    li{
        margin-bottom: 0.31rem;
        list-style: #626262;

        a{
            text-decoration: none;
            color: #626262;
            font-size: 1rem;
            font-weight: normal;

            transition: padding linear 1ms;
            &:hover{
                padding: 2px;
            }
        }        
    }

`
export const ContainerTypes = styled.ul`
    color: #626262;
    
    margin: 0.69rem 0 2.71rem 1.06rem;

    li{
        margin-bottom: 0.31rem;
        list-style: #626262;

        text-decoration: none;
        color: #626262;
        font-size: 1rem;
        font-weight: normal;     
        cursor: default;  
        
        transition: padding linear 1ms;
        &:hover{
            padding-left: 2px;
        }

        a{
            color: #626262;
            text-decoration:none;
        }
    }
`
export const ContainerLocalisation = styled.div`
    width: 12rem;
    a{
        color: #00A8A9;
        grid-column: 1/3;
        text-decoration: none;
    }

    span{
        color: #CB0D1F;
        text-transform: capitalize;
    }

    @media(max-width: 320px){
        margin: 0 0 .71rem 1.5rem
    }
    
`
