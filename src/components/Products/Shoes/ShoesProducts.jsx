import React, { useCallback } from "react"
import { HeaderTshits } from "../T-Shirts/HeaderTShirts"
import { HomeTShirts } from "../T-Shirts/styles"
import { Shoes } from "./Shoes"


export function ShoesProducts({valueColorFilter}) {
    const [toggleMenuProducts, setToggleMenuProducts] = React.useState(false)
    
    return (
        <HomeTShirts>
            <HeaderTshits handleToggleMenuProducts={setToggleMenuProducts} />
            <Shoes onChangeToggleMenuProduct={toggleMenuProducts} valueColorFilter={valueColorFilter} />
        </HomeTShirts>
    )
}