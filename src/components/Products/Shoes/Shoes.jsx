import React, { useEffect } from "react"
import { ContainerItems, ContainerPrice, ContainerTShirts, ImgProducts } from "../T-Shirts/styles"

export function Shoes({ onChangeToggleMenuProduct, valueColorFilter }) {

    const [shoes, setTShoes] = React.useState([])
    const [filter, setFilter] = React.useState(false)
    useEffect(() => {
        fetch('http://localhost:8888/api/V1/categories/3')
            .then(response => response.json())
            .then(data => {
                setTShoes(data.items)
            })
        
    }, [])

    useEffect(() => {
        if(valueColorFilter !== null) {
            setTShoes(shoes.filter(item => item.filter[0].color === valueColorFilter))
        }

    }, [valueColorFilter])
    

    return (
        <ContainerTShirts onChangeToggleMenuProduct={onChangeToggleMenuProduct} >

            {shoes?.map((shoe) => (
                <li key={shoe.id}>
                    <ContainerItems>
                        <ImgProducts src={shoe.image} alt={shoe.name} />
                        <p>{shoe.name}</p>
                        <ContainerPrice> <div>R$ {shoe.price}</div> </ContainerPrice>
                        <button> Comprar </button>
                    </ContainerItems>
                </li>
            ))}

        </ContainerTShirts>
    )
}
