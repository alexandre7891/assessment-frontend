import React, { useEffect } from "react"
import { Footer } from "../../Home/styles"
import { ContainerRoute } from "../../styles"
import { Localisation } from "../LocalidationPage"
import { SideNavigationProducts } from "../SideNavigationProducts"
import { PantsProduts } from "./PantsProducts"

export function ProductsCalcas ({onchangeBackgroundColor}){
    const route = window.location.pathname

    useEffect(()=>{
        if(route !== '/'){
            onchangeBackgroundColor(true)
        }else{
            onchangeBackgroundColor(false)
        }
    },[])

    return(
        <ContainerRoute>
            <Localisation onchangeBackgroundColor={onchangeBackgroundColor}/>
            <SideNavigationProducts />
            <PantsProduts/>
            <Footer/>
        </ContainerRoute>
    )
}