import React, { useEffect } from "react"
import { ContainerRoute } from "../../styles"
import { HeaderTshits } from "../T-Shirts/HeaderTShirts"
import { HomeTShirts } from "../T-Shirts/styles"
import { Pants } from "./Pants"

export function PantsProduts() {
    const [toggleMenuProducts, setToggleMenuProducts] = React.useState(false)
    return(
        <HomeTShirts>
             <HeaderTshits handleToggleMenuProducts={setToggleMenuProducts}/>
            <Pants onChangeToggleMenuProduct={toggleMenuProducts}/>
        </HomeTShirts>
    )
}