import React, { useCallback } from "react"
import { HeaderTshits } from "../../T-Shirts/HeaderTShirts"
import { HomeTShirts } from "../../T-Shirts/styles"
import { Social } from "./Social"

export function SocialProducts({valueColorFilter}) {
    const [toggleMenuProducts, setToggleMenuProducts] = React.useState(false)
    
    return (
        <HomeTShirts>
            <HeaderTshits handleToggleMenuProducts={setToggleMenuProducts} />
            <Social onChangeToggleMenuProduct={toggleMenuProducts} valueColorFilter={valueColorFilter} />
        </HomeTShirts>
    )
}