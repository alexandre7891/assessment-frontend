import React, { useEffect } from "react"
import { ContainerItems, ContainerPrice, ContainerTShirts, ImgProducts } from "../../T-Shirts/styles"

export function Social({ onChangeToggleMenuProduct, valueColorFilter }) {

    const [socials, setSocials] = React.useState([])
    const [filter, setFilter] = React.useState(false)
    useEffect(() => {
        fetch('http://localhost:8888/api/V1/categories/2')
            .then(response => response.json())
            .then(data => {
                setSocials(data.items.filter(item => item.name.match(/social/gi)))
            })

        fetch('http://localhost:8888/api/V1/categories/3')
            .then(response => response.json())
            .then(data => {
                data.items.filter(item=>
                    item.name.match(/social/gi)).length > 0 &&
                    setSocials([...socials, data.items.filter(item=> item.name.match(/social/gi))])
                    
            })

        fetch('http://localhost:8888/api/V1/categories/1')
            .then(response => response.json())
            .then(data => {
                data.items.filter(item=>
                    item.name.match(/social/gi)).length > 0 &&
                    setSocials([...socials, data.items.filter(item=> item.name.match(/social/gi))])
            })

    }, [])

    useEffect(() => {
        if (valueColorFilter !== null) {
            setFilter(true)
            setSocials(socials.filter(item => item.filter[0].color === valueColorFilter))
        }

    }, [valueColorFilter])


    return (
        <ContainerTShirts onChangeToggleMenuProduct={onChangeToggleMenuProduct} >

            {socials?.map((social) => (
                <li key={social.id}>
                    <ContainerItems>
                        <ImgProducts src={social.image} alt={social.name} />
                        <p>{social.name}</p>
                        <ContainerPrice> <div>R$ {social.price}</div> </ContainerPrice>
                        <button> Comprar </button>
                    </ContainerItems>
                </li>
            ))}

        </ContainerTShirts>
    )
}
