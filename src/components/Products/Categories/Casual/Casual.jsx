import React, { useEffect } from "react"
import { ContainerItems, ContainerPrice, ContainerTShirts, ImgProducts } from "../../T-Shirts/styles"

export function Casual({ onChangeToggleMenuProduct, valueColorFilter }) {

    const [casuals, setCasual] = React.useState([])
    const [filter, setFilter] = React.useState(false)
    useEffect(() => {
        fetch('http://localhost:8888/api/V1/categories/3')
            .then(response => response.json())
            .then(data => {
                setCasual(data.items.filter(item => item.name.match(/casual/gi)))
            })

        fetch('http://localhost:8888/api/V1/categories/2')
            .then(response => response.json())
            .then(data => {
                data.items.filter(item=>
                    item.name.match(/casual/gi)).length > 0 &&
                    setCasual([...casuals, data.items.filter(item=> item.name.match(/casual/gi))])
                    
            })

        fetch('http://localhost:8888/api/V1/categories/1')
            .then(response => response.json())
            .then(data => {
                data.items.filter(item=>
                    item.name.match(/casual/gi)).length > 0 &&
                    setCasual([...casuals, data.items.filter(item=> item.name.match(/casual/gi))])
            })

    }, [])

    useEffect(() => {
        if (valueColorFilter !== null) {
            setFilter(true)
            setCasual(casuals.filter(item => item.filter[0].color === valueColorFilter))
        }

    }, [valueColorFilter])


    return (
        <ContainerTShirts onChangeToggleMenuProduct={onChangeToggleMenuProduct} >

            {casuals?.map((casual) => (
                <li key={casual.id}>
                    <ContainerItems>
                        <ImgProducts src={casual.image} alt={casual.name} />
                        <p>{casual.name}</p>
                        <ContainerPrice> <div>R$ {casual.price}</div> </ContainerPrice>
                        <button> Comprar </button>
                    </ContainerItems>
                </li>
            ))}

        </ContainerTShirts>
    )
}
