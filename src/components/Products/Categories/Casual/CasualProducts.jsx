import React, { useCallback } from "react"
import { HeaderTshits } from "../../T-Shirts/HeaderTShirts"
import { HomeTShirts } from "../../T-Shirts/styles"
import { Casual } from "./Casual"

export function CasualProducts({valueColorFilter}) {
    const [toggleMenuProducts, setToggleMenuProducts] = React.useState(false)
    
    return (
        <HomeTShirts>
            <HeaderTshits handleToggleMenuProducts={setToggleMenuProducts} />
            <Casual onChangeToggleMenuProduct={toggleMenuProducts} valueColorFilter={valueColorFilter} />
        </HomeTShirts>
    )
}