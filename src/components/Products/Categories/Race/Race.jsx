import React, { useEffect } from "react"
import { ContainerItems, ContainerPrice, ContainerTShirts, ImgProducts } from "../../T-Shirts/styles"

export function Rance({ onChangeToggleMenuProduct, valueColorFilter }) {

    const [races, setRaces] = React.useState([])
    
    useEffect(() => {

        fetch('http://localhost:8888/api/V1/categories/3')
            .then(response => response.json())
            .then(data => {
                setRaces(data.items.filter(item => item.name.match(/corrida/gi)))
            })

        fetch('http://localhost:8888/api/V1/categories/2')
            .then(response => response.json())
            .then(data => {
                data.items.filter(item =>
                    item.name.match(/corrida/gi)).length > 0 &&
                    setRaces([...races, data.items.filter(item => item.name.match(/corrida/gi))])
            })


        fetch('http://localhost:8888/api/V1/categories/1')
            .then(response => response.json())
            .then(data => {
                data.items.filter(item =>
                    item.name.match(/corrida/gi)).length > 0 &&
                    setRaces([...races, data.items.filter(item => item.name.match(/corrida/gi))])
            })

    }, [])

    useEffect(() => {
        if (valueColorFilter !== null) {
            setRaces(races.filter(item => item.filter[0].color === valueColorFilter))
        }

    }, [valueColorFilter])

console.log(races)
    return (
        <ContainerTShirts onChangeToggleMenuProduct={onChangeToggleMenuProduct} >

            {races?.map((race) => (
                <li key={race.id}>
                    <ContainerItems>
                        <ImgProducts src={race.image} alt={race.name} />
                        <p>{race.name}</p>
                        <ContainerPrice>
                            {race.specialPrice ?
                                (<>
                                    <span>R$ {race.price}</span>
                                    <div>R$ {race.specialPrice}</div>
                                </>) : (<div> R$ {race.price}</div>)}
                        </ContainerPrice>
                        <button> Comprar </button>
                    </ContainerItems>
                </li>
            ))}

        </ContainerTShirts>
    )
}
