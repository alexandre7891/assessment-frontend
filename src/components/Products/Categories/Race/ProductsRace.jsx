import React, { useEffect } from "react"
import { Footer } from "../../../Home/styles"
import { ContainerRoute } from "../../../styles"
import { Localisation } from "../../LocalidationPage"
import { SideNavigationProducts } from "../../SideNavigationProducts"
import { RanceProducts } from "./RaceProducts"

export function ProductsRance ({onchangeBackgroundColor}){
    const route = window.location.pathname
    const [valueColorFilter, setValueColorFilter]= React.useState(null)

    useEffect(()=>{
        if(route !== '/'){
            onchangeBackgroundColor(true)
        }else{
            onchangeBackgroundColor(false)
        }
    },[])

    return(
        <ContainerRoute>
            <Localisation onchangeBackgroundColor={onchangeBackgroundColor} />
            <SideNavigationProducts ShowColors={route} GetColorFilter={setValueColorFilter}/>
            <RanceProducts valueColorFilter={valueColorFilter}/>
            <Footer/>
        </ContainerRoute>
    )
}