import React, { useCallback } from "react"
import { HeaderTshits } from "../../T-Shirts/HeaderTShirts"
import { HomeTShirts } from "../../T-Shirts/styles"
import { Rance } from "./Race"



export function RanceProducts({valueColorFilter}) {
    const [toggleMenuProducts, setToggleMenuProducts] = React.useState(false)
    
    return (
        <HomeTShirts>
            <HeaderTshits handleToggleMenuProducts={setToggleMenuProducts} />
            <Rance onChangeToggleMenuProduct={toggleMenuProducts} valueColorFilter={valueColorFilter} />
        </HomeTShirts>
    )
}