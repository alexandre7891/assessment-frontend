import React from "react"
import { HeaderTshits } from "../../T-Shirts/HeaderTShirts"
import { HomeTShirts } from "../../T-Shirts/styles"
import { Clothes } from "./Clothes"

export function ClothesProducts() {
    const [toggleMenuProducts, setToggleMenuProducts] = React.useState(false)
    
    return(
        <HomeTShirts>
            <HeaderTshits handleToggleMenuProducts={setToggleMenuProducts}/>
            <Clothes onChangeToggleMenuProduct={toggleMenuProducts} />
        </HomeTShirts>
    )
}