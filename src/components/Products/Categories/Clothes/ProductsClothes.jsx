import React, { useEffect } from "react"
import { Footer } from "../../../Home/styles"
import { ContainerRoute } from "../../../styles"
import { Localisation } from "../../LocalidationPage"
import { SideNavigationProducts } from "../../SideNavigationProducts"
import { ClothesProducts } from "./ClothesProducts"


export function ProductsClothes ({onchangeBackgroundColor}){
    const route = window.location.pathname

    useEffect(()=>{
        if(route !== '/'){
            onchangeBackgroundColor(true)
        }else{
            onchangeBackgroundColor(false)
        }
    },[])

    return(
        <ContainerRoute>
            <Localisation onchangeBackgroundColor={onchangeBackgroundColor}/>
            <SideNavigationProducts />
            <ClothesProducts/>
            <Footer/>
        </ContainerRoute>
    )
}