import React, { useEffect } from "react"
import { ContainerItems, ContainerPrice, ContainerTShirts, ImgProducts } from "../../T-Shirts/styles"

export function Clothes({onChangeToggleMenuProduct}) {

    const [tShirts, setTShirts] = React.useState([])
    const [pants, setPants] = React.useState([])

    useEffect(() => {
        fetch('http://localhost:8888/api/V1/categories/1')
            .then(response => response.json())
            .then(data => {
                setTShirts(data.items)
            })

        fetch('http://localhost:8888/api/V1/categories/2')
            .then(response => response.json())
            .then(data => {
                setPants(data.items)
            })
    }, [])

    return (
        <ContainerTShirts onChangeToggleMenuProduct={onChangeToggleMenuProduct} >

            {tShirts?.map((tshirt) => (
                <li key={tshirt.id}>
                    <ContainerItems>
                        <ImgProducts src={tshirt.image} alt={tshirt.name} />
                        <p>{tshirt.name}</p>
                        <ContainerPrice> <div>R$ {tshirt.price}</div> </ContainerPrice>
                        <button> Comprar </button>
                    </ContainerItems>
                </li>
            ))}

            {pants?.map((pant) => (
                <li key={pant.id}>
                    <ContainerItems>
                        <ImgProducts src={pant.image} alt={pant.name} />
                        <p> {pant.name} </p>
                        <ContainerPrice>
                            {pant.specialPrice ?
                                (<>
                                    <span>R$ {pant.price}</span>
                                    <div>R$ {pant.specialPrice}</div>
                                </>) : (<div> R$ {pant.price}</div>)}
                        </ContainerPrice>
                        <button>COMPRAR</button>
                    </ContainerItems>
                </li>
            ))}
        </ContainerTShirts>
    )
}
