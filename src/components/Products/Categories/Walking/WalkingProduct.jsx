import React, { useCallback } from "react"
import { HeaderTshits } from "../../T-Shirts/HeaderTShirts"
import { HomeTShirts } from "../../T-Shirts/styles"
import { Walking } from "./Walking"


export function WalkingProducts({valueColorFilter}) {
    const [toggleMenuProducts, setToggleMenuProducts] = React.useState(false)
    
    return (
        <HomeTShirts>
            <HeaderTshits handleToggleMenuProducts={setToggleMenuProducts} />
            <Walking onChangeToggleMenuProduct={toggleMenuProducts} valueColorFilter={valueColorFilter} />
        </HomeTShirts>
    )
}