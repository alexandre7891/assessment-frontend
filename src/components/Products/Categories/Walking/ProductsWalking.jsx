import React, { useEffect } from "react"
import { Footer } from "../../../Home/styles"
import { ContainerRoute } from "../../../styles"
import { Localisation } from "../../LocalidationPage"
import { SideNavigationProducts } from "../../SideNavigationProducts"
import { WalkingProducts } from "./WalkingProduct"


export function ProductsWalking ({onchangeBackgroundColor}){
    const route = window.location.pathname
    const [valueColorFilter, setValueColorFilter]= React.useState(null)

    useEffect(()=>{
        if(route !== '/'){
            onchangeBackgroundColor(true)
        }else{
            onchangeBackgroundColor(false)
        }
    },[])

    return(
        <ContainerRoute>
            <Localisation onchangeBackgroundColor={onchangeBackgroundColor} />
            <SideNavigationProducts ShowColors={route} GetColorFilter={setValueColorFilter}/>
            <WalkingProducts valueColorFilter={valueColorFilter}/>
            <Footer/>
        </ContainerRoute>
    )
}