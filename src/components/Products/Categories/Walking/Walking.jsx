import React, { useEffect } from "react"
import { ContainerItems, ContainerPrice, ContainerTShirts, ImgProducts } from "../../T-Shirts/styles"

export function Walking({ onChangeToggleMenuProduct, valueColorFilter }) {

    const [shoes, setTShoes] = React.useState([])
    
    useEffect(() => {
        fetch('http://localhost:8888/api/V1/categories/2')
            .then(response => response.json())
            .then(data => {
                setTShoes(data.items.filter(item => item.name.match(/caminhada/gi)))
            })

        fetch('http://localhost:8888/api/V1/categories/1')
            .then(response => response.json())
            .then(data =>
                data.items.filter(item =>
                    item.name.match(/caminhada/gi)).length > 0 &&
                setTShoes(...shoes, data.items.filter(item => item.name.match(/caminhada/gi)))
            )


        fetch('http://localhost:8888/api/V1/categories/3')
            .then(response => response.json())
            .then(data =>
                data.items.filter(item =>
                    item.name.match(/caminhada/gi)).length > 0 &&
                setTShoes(...shoes, data.items.filter(item => item.name.match(/caminhada/gi)))
            )
    }, [])

    useEffect(() => {
        if (valueColorFilter !== null) {
            setTShoes(shoes.filter(item => item.filter[0].color === valueColorFilter))
        }

    }, [valueColorFilter])

    return (
        <ContainerTShirts onChangeToggleMenuProduct={onChangeToggleMenuProduct} >

            {shoes?.map((shoe) => (
                <li key={shoe.id}>
                    <ContainerItems>
                        <ImgProducts src={shoe.image} alt={shoe.name} />
                        <p>{shoe.name}</p>
                        <ContainerPrice> <div>R$ {shoe.price}</div> </ContainerPrice>
                        <button> Comprar </button>
                    </ContainerItems>
                </li>
            ))}

        </ContainerTShirts>
    )
}
