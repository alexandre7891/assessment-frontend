import React, { useEffect } from "react"
import MenuSquareImg from "../../../Icons/menu-quadrado.png"
import MenuBarImg from "../../../Icons/barra-de-menu.png"
import { HomeTShirts, ImgMenu, Line } from "./styles"

export function HeaderTshits({handleToggleMenuProducts}){
    const route = window.location.pathname.substring(1)
    
    return(
        <HomeTShirts>
            <p>{route==='calcas' ? 'Calças': (route==='calcados' ? 'calçados': route )}</p>
            <Line/>
            <div>
                <div>
                    <ImgMenu src={MenuSquareImg} alt="menu quadrado" onClick={()=>handleToggleMenuProducts(false)}/>
                    <ImgMenu src={MenuBarImg} alt="barra de menu" onClick={()=>handleToggleMenuProducts(true)}/>
                </div>

                <div>
                    <span>ORDENAR POR</span>
                    <select>
                        <option value="unselected">Selecione</option>
                        <option value="price">Preço</option>
                        <option value="Feminina">Feminina</option>
                        <option value="Masculino">Masculino</option>
                    </select>
                </div>
            </div>
        </HomeTShirts>
    )
}