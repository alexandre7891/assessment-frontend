import styled, { css } from "styled-components"

export const HomeTShirts = styled.div`
    p{
        color: #ED1A39;
        font-size: 2rem;
        margin-bottom: 0.5rem;
        text-transform: capitalize;
    }
        
    grid-row: 2/4;
    
    div{
        &:nth-child(3){
            display: flex;
            justify-content: space-between;
            align-items: center;
            span{
                margin-right: 0.83rem;
                color: #808185;
                font-size: 0.68rem;
            }
        }

        select{
            width: 10.93rem;
            border-color: #E2DEDC;
            background: #FFFFFF 0% 0% no-repeat padding-box;
        }

    }    

    @media(max-width: 768px){
        width: 35.9rem;
    }

    @media(max-width: 320px){
        width: 20rem;
        margin: auto;
        padding: 1.15rem 0;
    }
`

export const Line = styled.div`
    height: 1px;
    background: #E2DEDC;
    margin-bottom: 0.5rem;
    @media(max-width: 320px){
        display: none;
    }
`

export const ImgMenu = styled.img`
    width: 1.53rem;
    height: 1.25rem;
    margin-right: 0.72rem;
    
    @media(max-width: 320px){
        display: none;
    }
`

export const ContainerTShirts = styled.ul`

    display: flex;
    flex-wrap: wrap;
    justify-content: left;

    margin-top:2.375rem;

    li{ list-style:none; margin-top:1rem;}

    ${({onChangeToggleMenuProduct})=>
        onChangeToggleMenuProduct && css`
        flex-direction: column;
        justify-content: center;
        `
    }
    
    @media(max-width: 768px){
        justify-content: left;
    }

`

export const ContainerItems = styled.div`
    height: 20.71rem;
    width: 13.125rem;
    
    background: #FFFFFF 0% 0% no-repeat padding-box;

    text-align: center;
    margin-right: .5rem;
    margin-bottom: 1.25rem;

    p{
        color: #626262;
        font-size: 1rem;
        margin-bottom: 1.5rem;
    }

    button{
        background: #00A8A9 0% 0% no-repeat padding-box;
        color: #FFFFFF;
        border-radius: 5px;
        border: none;

        font-weight: 800;
        font-size: 1.125rem;

        width: 13.125rem;
        height: 2.5rem;

        transition: all linear 0.2s;

        &:hover{
            background-color: #7DD7D8;
        }
    }
    
    @media (max-width: 768px){
        width: 10.25rem;
        margin-right: 1.7rem;
        button{
            width: 10.25rem;
            
        }
    }

    @media(max-width: 320px){
        width: 8.29rem;
        height: 17rem;
        button{
            display: none;
        }
    }
`
export const ContainerPrice = styled.ul`
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    
    width: 9.12rem;
    margin: 0 0 0.6rem 2rem;

    span{
        color: #808185;
        font-size: 0.93rem;
        text-decoration: line-through;
        margin-right: 5px;
    }

    div{
            color: #1E2B50;
            font-size: 1.31rem;
            font-weight: 800;
    }

    @media(max-width: 768px){
        width: 9.68rem;
        margin: 0 0 0 0.5rem;

        span{
            margin-right: 0;
        }
    }

    @media(max-width: 320px){
        display: block;
        width: 8.68rem;
    }

`

export const ImgProducts = styled.img`
    width: 12.25rem;
    height: 13.43rem;

    padding: 0.5rem 0.49rem;

    margin-bottom: 0.5rem;
    border: 1px solid #E2DEDC;

    
    @media (max-width: 768px){
        width: 10.18rem;
        height: 11.19rem;
        padding: 0.3rem 0.39rem;

    }

    @media(max-width: 320px){
        width: 8.29rem;
        height: 9.125rem;
    }
`