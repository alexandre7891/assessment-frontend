import React from "react"
import { TShirts } from "./TShits"
import { HeaderTshits } from "./HeaderTShirts"
import { HomeTShirts } from "./styles"

export function ProductCamiseta() {
    const [toggleMenuProducts, setToggleMenuProducts] = React.useState(false)
    return (
        <HomeTShirts>
            <HeaderTshits handleToggleMenuProducts={setToggleMenuProducts} />
            <TShirts onChangeToggleMenuProduct={toggleMenuProducts} />
        </HomeTShirts>
    )
}