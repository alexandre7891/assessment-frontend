import React, { useEffect } from "react"
import { HeaderTshits } from "./HeaderTShirts"
import { ContainerItems, ContainerPrice, ContainerTShirts, ImgProducts } from "./styles"

export function TShirts({ onChangeToggleMenuProduct }) {

    const [tShirts, setTShirs] = React.useState([])

    useEffect(() => {
        fetch('http://localhost:8888/api/V1/categories/1')
            .then(response => response.json())
            .then(data => {
                setTShirs(data.items)
            })
    }, [])

    return (
        <ContainerTShirts onChangeToggleMenuProduct={onChangeToggleMenuProduct} >

            {tShirts?.map((tshirt) => (
                <li key={tshirt.id}>
                    <ContainerItems>
                        <ImgProducts src={tshirt.image} alt={tshirt.name} />
                        <p>{tshirt.name}</p>
                        <ContainerPrice>
                            <div>R$ {tshirt.price}</div>
                        </ContainerPrice>
                        <button> Comprar </button>
                    </ContainerItems>
                </li>
            ))}

        </ContainerTShirts>
    )
}