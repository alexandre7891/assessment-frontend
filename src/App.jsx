import React from "react"
import { BrowserRouter } from "react-router-dom"
import { GlobalStyle } from "./components/styles"
import { Rotas } from "./Routes"

export function App() {
    return (
        <BrowserRouter>
           <Rotas/>
            <GlobalStyle />
        </BrowserRouter>

    )
}