# Framework Escolhido?
Escolhi o React JS, pois tenho mais experiência com o mesmo e ele proporciona uma experiência mais agradável para usuário, na hora das trocas de telas.

## Tecnologias Usadas e soluções
Para este desafio o React JS foi escolhido porque permite uma experiência interessante para o usuário, 
principalmente na parte visual, pois não tenhos um carregamento total de tela toda vez que quermos acessar algum link interno. 
Sendo assim, o usuário terá uma experiência parecido a um app mobile. 

Já de um ponto de vista de desenvolvimento, a escolha desse framework deu-se pela praticidade para reutilizar componentes
em todo o código e o compartilhamento de propriedades e estados entre estes componentes. Assim também, como duas renderizações condicionais,
isto é, um determinado compomente poderá ser alterado quando uma determina propriedade propriedade for alterada ou algum evento ocorrer.

Esta possibilidade de rendenrização condicional foi aplicada para a funcionalidades do filtro.

## Requisitos
- Design responsivo nos breakpoints 320px, 768px, 1024px e 1440px <strong> ok </strong> 
- Suporte para IE, Chrome, Safari, Firefox <strong> ok </strong> 
- Semântica <strong> ok </strong> 

## Diferenciais
- Uso de pré-processadores CSS (Sass, Less) <strong> ok </strong>  <strong> Para este caso, usei o Styled-Componets, o qual têm muitas semelhanças com o Sass </strong>
- Acessibilidade <strong> ok </strong> 
- SEO <strong> ok </strong> 
- Performance <strong> ok </strong> 
- Fazer os filtros da sidebar funcionarem através de Javascript <strong> ok </strong> 
- Utilizar alguma automatização (Grunt, Gulp, ...) 

## Como iniciar o desenvolvimento
- Instale o [npm](https://nodejs.org/en/download/)
- Instale as dependências
```
npm install ou yarn
```
- Rode a aplicação
```
Será necessário abrir dois terminais e roda os seguintes comandos na seguinte ordem:
npm start
yarn dev
```
- Acesse http://localhost:8080